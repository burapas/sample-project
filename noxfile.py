import nox

@nox.session(python=["3.6"])
def lint(session):
    """Lint the module."""
    # session.install("flake8")
    # session.run("flake8")

@nox.session(python=["3.6"])
def tests(session):
    """Run the unit test suit."""
    session.install("mock", "pytest")
    session.install("-e", ".")

    session.run(
        "pytest",
        "--quiet",
        "tests.py",
    )

@nox.session(python=["3.6"])
def docs(session):
    session.install(".")
    session.install("sphinx", "sphinx-autobuild")
    session.chdir("docs")
    session.run("rm", "-rf", "build/", external=True)
    sphinx_args = ["-W", "source", "build/html"]
    if "serve" in session.posargs:
        session.run("sphinx-autobuild", *sphinx_args)
    else:
        session.run("sphinx-build", *sphinx_args)