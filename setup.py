import setuptools

setuptools.setup(
    name="sample",
    version="0.0.1",
    description="a sample package",
    # author="",
    # author_email="",
    # url="",
    # license="",
    # python_requires=">=3.6",
    # install_requires=[],
    packages=setuptools.find_packages(
        exclude=[
            "docs", "tests", "test_data"
        ]
    )
    # classifiers=[]
)

#tox or nox
#build sphinx docs during testing

#“Code tells you how; Comments tell you why.”